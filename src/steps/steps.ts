import { Given, Then, When } from '@cucumber/cucumber';
import Active from '../pages/active.page';

const pages = {
    active: Active,
};

Given('I am on the {string} page', async (page: string) => {
    await pages[page]?.open();
});

When('I add a task', async function () {
    this.addTask = `task${new Date().getTime()}`;
    await Active.addTask(this.addTask);
});

When('I delete the task', async function () {
    await Active.deleteTask(this.addTask);
});

Then('the task is add', async function () {
    const task = await Active.getTaskWithText(this.addTask);
    expect(task).toHaveLength(1);
    expect(task[0]).toHaveText(this.addTask);
});

Then('the task is delete', async function () {
    const task = await Active.getTaskWithText(this.addTask);
    expect(task).toHaveLength(0);
});
