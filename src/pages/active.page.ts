import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class Active extends Page {
    /**
     * overwrite specifc options to adapt it to page object
     */
    open() {
        return super.open('/#/active');
    }

    get clearCompleteBtn() {
        return 'button.clear-completed';
    }

    async addTask(title: string) {
        await (await $('.new-todo')).setValue(title);
        await browser.keys(['Enter']);
    }

    async deleteTask(title: string) {
        const task = await this.getTaskWithText(title);
        await (await task[0].$('.destroys')).click();
    }

    async getTaskWithText(title: string) {
        return await Promise.all(
            (await $$('li.todo')).filter(async (todo) => {
                return (await todo.getText()).includes(title);
            }),
        );
    }
}

export default new Active();
