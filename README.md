# E2E tests with WebDriverIO and Cucumber

![pipeline](https://gitlab.com/pdelpy/wdio-technical-test/badges/master/pipeline.svg)

This is a demonstration project of integration tests. In this project the user try to connect to an app.  
These tests are developed in TypeScript with [WebDriverIO V7](http://webdriver.io/) and [Cucumber](https://cucumber.io/)

## Features

-   [TypeScript](https://www.typescriptlang.org/docs/home.htm)
-   [Expect-webdriverio](https://github.com/webdriverio/expect-webdriverio)
-   [Browserstack](https://www.browserstack.com/)

## Requirements
-   git
-   node >= 12.18.x - [how to install Node](https://nodejs.org/en/download/)
-   npm >= 6.14.x - [how to install NPM](https://www.npmjs.com/get-npm)
-   openJDK >= 11.x.x - [how to install JDK](https://openjdk.java.net/install/)

## Getting Started

Install the dependencies:

```bash
npm install
```

Run e2e tests:

```bash
npm run test
```

Run e2e tests with a tag (ex: `@test`):
Add a tag into the feature file and launch the following command

```bash
TAGS="@test" npm run test
```

## Reports

### Allure

Run this command to generate the allure report in the directory `.tmp/allure-report`:

```bash
npm run report:generate
```

You can run this command to generate the report and start a server on your machine and open report on the browser:

```bash
npm run report
```

## Tslint

Run to format the code:

```bash
npm run lint
```

## Technical test

🎯 Target: Automated the [todo](https://todomvc.com/examples/vue/) site with some keys features (Add, Delete, Edit, Complete).

**Your Mission**

1. Install the project and run it => 📷 take the screenshot 😀
2. Fix some errors =>  📷 agains take the screenshot 😀
3. Create a gherkins for the complete feature
4. Implement the created gherkins steps (Thinking Page Object Model)
5. Propose a Merge Request (push you works 📦 😀) to the repo (branch naming convention 'test/my-name')

**Extra: Be proud do More 💪**

6. Create and Implement Edit feature
7. Add the report to the execution of the pipeline (use the .gitlab-ci.yml)