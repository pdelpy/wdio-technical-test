const config = {
    runner: 'local',
    specs: ['./src/features/**/*.feature'],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 10,
    capabilities: [
        {
            maxInstances: 5,
            browserName: 'chrome',
            acceptInsecureCerts: true,
        },
    ],
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: process.env.DEBUG ? 'trace' : 'error',
    bail: 0,
    baseUrl: 'https://todomvc.com/examples/vue/',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ['selenium-standalone'],
    framework: 'cucumber',
    reporters: [
        'spec',
        [
            'cucumberjs-json',
            {
                jsonFolder: '.tmp/cucumber-results',
                language: 'en',
            },
        ],
        [
            'allure',
            {
                outputDir: '.tmp/allure-results',
                disableWebdriverStepsReporting: true,
                disableWebdriverScreenshotsReporting: false,
                useCucumberStepReporter: true,
            },
        ],
    ],
    cucumberOpts: {
        require: ['./src/steps/**/*.ts'],
        backtrace: false,
        requireModule: [],
        dryRun: false,
        failFast: false,
        format: ['pretty'],
        snippets: true,
        source: true,
        profile: [],
        strict: false,
        tagExpression: process.env.TAGS,
        retry: process.env.RETRY ? process.env.RETRY : 0,
        timeout: 60000,
        ignoreUndefinedDefinitions: false,
    },
    afterStep: async function (
        test,
        context,
        { error, result, duration, passed, retries },
    ) {
        if (error) {
            await browser.takeScreenshot();
        }
    },
};

exports.config = {
    ...config,
};
